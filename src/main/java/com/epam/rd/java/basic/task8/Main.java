package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void main(String[] args) throws Exception {
		// DOM parser
		DOMController domController = new DOMController(args[0]);
		domController.parseXML(true);
		domController.saveToXML();

		// STAX parser
		STAXController stAXController = new STAXController(args[0]);
		stAXController.parse();
		stAXController.writeXML();

		// Sax parser
		SAXController saxController = new SAXController(args[0]);
		saxController.parse(true);
		saxController.writeXML();
	}

}
